package ssii.csi2;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public class Main {
    private static final String[] hexDigits = { "0", "1", "2", "3","4", "5", "6", "7","8", "9", "a", "b", "c", "d", "e", "f" };
    private static final String algorithm = "HmacSHA1";

    private static final String message = "23456789 987654 300";
    private static final String diggest = "eef039c72a544d5b12e30f8022eb59cc96895f02";

    private static String calculateHMAC(String data, byte [] key) throws NoSuchAlgorithmException, InvalidKeyException {
        SecretKeySpec signingKey = new SecretKeySpec(key, algorithm);
        Mac mac = Mac.getInstance(algorithm);
        mac.init(signingKey);
        return byteArrayToHexString(mac.doFinal(data.getBytes()));
    }

    public static void main(String argv[]) throws InvalidKeyException, NoSuchAlgorithmException {
        long startTime = System.nanoTime();
        boolean found = false;
        long usedKey = 0;

        for (long i = 0; i < 16777216; i++) {
            byte [] key = generateKey24(i);
            String hmac = calculateHMAC(message, key);
            //System.out.println(hmac + " : " + i);

            if (hmac.compareTo(diggest) == 0) {
                usedKey = i;
                found = true;
            }
        }

        float executionTimeInSeconds = ((float) (System.nanoTime() - startTime)) / 1000000;
        long keysTried = found ? usedKey : 16777216;
        float keysPerSecond = ((float) keysTried) / executionTimeInSeconds;

        System.out.println("Tried " + keysTried + " keys in " + executionTimeInSeconds + " seconds (" + keysPerSecond + " keys per second).");
        System.out.println("Found: " + (found ? "yes" : "no"));
        if (found) {
            System.out.println("The key was " + usedKey);
        }
    }

    private static byte [] generateKey24(long keyNumber) {
        // http://stackoverflow.com/questions/1936857/convert-integer-into-byte-array-java
        if (keyNumber < 0 || keyNumber > 16777216) {
            throw new IllegalArgumentException("keyNumber is out of range.");
        }

        byte [] rval = new byte[3];
        rval[0] = (byte) (keyNumber >> 16);
        rval[1] = (byte) (keyNumber >> 8);
        rval[2] = (byte) (keyNumber);

        return rval;
    }

    /**
     * Convierte un array de bytes en una cadena hexadecimal.
     *
     * @param hash Bytes.
     * @return Cadena hexadecimal que representa al array de bytes.
     */
    private static String byteArrayToHexString(byte[] hash) {
        String rval = "";
        for (byte aHash : hash) {
            rval += byteToHexString(aHash);
        }
        return rval;
    }
    /**
     * Convierte un byte a cadena hexadecimal.
     *
     * @param hash Byte
     * @return Cadena hexadecimal que representa el byte.
     */
    private static String byteToHexString(byte hash) {
        int n = hash;
        if (n < 0) {
            n=256 +n;
        }
        int d1 = n / 16;
        int d2 = n % 16;
        return hexDigits[d1] + hexDigits[d2];
    }
}
