# CSI2 #

Se trata de identificar, si es posible, la clave utilizada para generar el hMac de un determinado
mensaje. El código es muy simple y no acepta argumentos; hay una serie de variables estáticas
finales en la clase `Main` donde se puede especificar el mensaje y el algoritmo (probado con SHA1).

La longitud de las claves es de 24 bits. Es fácil adaptarlo para claves mayores.